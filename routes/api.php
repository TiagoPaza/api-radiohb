<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::get('player', 'PlayerStatusController@status');

Route::resource('news', 'NewsController', ['except' => ['create', 'edit', 'delete']]);
Route::resource('charges', 'ChargesController', ['except' => ['create', 'edit', 'delete']]);
Route::resource('users', 'UsersController', ['except' => ['create', 'edit', 'delete']]);
Route::resource('communities', 'CommunitiesController', ['except' => ['create', 'edit', 'delete']]);
