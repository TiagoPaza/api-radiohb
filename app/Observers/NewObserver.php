<?php

namespace App\Observers;

use App\News;
use Spatie\ResponseCache\ResponseCache;

class NewObserver
{
    protected $cache;

    /**
     * @param ResponseCache $cache
     */
    public function __construct(ResponseCache $cache)
    {
        $this->cache = $cache;
    }

    /**
     * Listen to the Order created event.
     *
     * @param News $new
     * @return void
     */
    public function created(News $new)
    {
        $this->cache->flush();
    }

    /**
     * Listen to the Order created event.
     *
     * @param New $new
     * @return void
     */
    public function updated(News $new)
    {
        $this->cache->flush();
    }

    /**
     * Listen to the Order deleted event.
     *
     * @param New $new
     * @return void
     */
    public function deleted(News $new)
    {
        $this->cache->flush();
    }
}
