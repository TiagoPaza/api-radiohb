<?php

namespace App\Observers;

use App\Users;
use Spatie\ResponseCache\ResponseCache;

class UserObserver
{
    protected $cache;

    /**
     * @param ResponseCache $cache
     */
    public function __construct(ResponseCache $cache)
    {
        $this->cache = $cache;
    }

    /**
     * Listen to the Order created event.
     *
     * @param User $user
     * @return void
     */
    public function created(User $user)
    {
        $this->cache->flush();
    }

    /**
     * Listen to the Order created event.
     *
     * @param User $user
     * @return void
     */
    public function updated(User $user)
    {
        $this->cache->flush();
    }

    /**
     * Listen to the Order deleted event.
     *
     * @param User $user
     * @return void
     */
    public function deleted(User $user)
    {
        $this->cache->flush();
    }
}
