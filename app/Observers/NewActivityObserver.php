<?php

namespace App\Observers;

use App\NewActivity;
use Spatie\ResponseCache\ResponseCache;

class NewActivityObserver
{
    protected $cache;

    /**
     * @param ResponseCache $cache
     */
    public function __construct(ResponseCache $cache)
    {
        $this->cache = $cache;
    }

    /**
     * Listen to the NewActivity created event.
     *
     * @param NewActivity $newActivity
     * @return void
     */
    public function created(NewActivity $newActivity)
    {
        $this->cache->flush();
    }

    /**
     * Listen to the NewActivity created event.
     *
     * @param NewActivity $newActivity
     * @return void
     */
    public function updated(NewActivity $newActivity)
    {
        $this->cache->flush();
    }

    /**
     * Listen to the NewActivity deleted event.
     *
     * @param NewActivity $newActivity
     * @return void
     */
    public function deleted(NewActivity $newActivity)
    {
        $this->cache->flush();
    }
}
