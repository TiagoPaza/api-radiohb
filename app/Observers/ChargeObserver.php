<?php

namespace App\Observers;

use App\Charge;
use Spatie\ResponseCache\ResponseCache;

class ChargeObserver
{
    protected $cache;

    /**
     * @param ResponseCache $cache
     */
    public function __construct(ResponseCache $cache)
    {
        $this->cache = $cache;
    }

    /**
     * Listen to the Order created event.
     *
     * @param Charge $chage
     * @return void
     */
    public function created(Charge $chage)
    {
        $this->cache->flush();
    }

    /**
     * Listen to the Order created event.
     *
     * @param New $chage
     * @return void
     */
    public function updated(Charge $chage)
    {
        $this->cache->flush();
    }

    /**
     * Listen to the Order deleted event.
     *
     * @param New $chage
     * @return void
     */
    public function deleted(Charge $chage)
    {
        $this->cache->flush();
    }
}
