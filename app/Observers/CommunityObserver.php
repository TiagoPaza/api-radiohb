<?php

namespace App\Observers;

use App\Community;
use Spatie\ResponseCache\ResponseCache;

class CommunityObserver
{
    protected $cache;

    /**
     * @param ResponseCache $cache
     */
    public function __construct(ResponseCache $cache)
    {
        $this->cache = $cache;
    }

    /**
     * Listen to the Order created event.
     *
     * @param Community $community
     * @return void
     */
    public function created(Community $community)
    {
        $this->cache->flush();
    }

    /**
     * Listen to the Order created event.
     *
     * @param Community $community
     * @return void
     */
    public function updated(Community $community)
    {
        $this->cache->flush();
    }

    /**
     * Listen to the Order deleted event.
     *
     * @param Community $community
     * @return void
     */
    public function deleted(Community $community)
    {
        $this->cache->flush();
    }
}
