<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class NewActivity extends Model
{
    protected $table = 'noticias_atividades';
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * We always want new info for orders so eager load to save n+1 queries.
     *
     * @var array
     */
    protected $with = ['news'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['data'];

    public function news()
    {
        return $this->belongsTo(News::class, 'id_noticia');
    }
}
