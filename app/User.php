<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'acp_usuarios';
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    protected $fillable = [
        'nick', 'senha', 'ativado',
        'ativado_autor', 'ativado_data', 'cargos',
        'cargos_e', 'facebook', 'instagram',
        'twitter', 'link_quarto', 'nome',
        'skype', 'programa', 'advert',
        'chat_activity', 'acesso_data', 'acesso_ip',
        'autor', 'status'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['data'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'senha', 'acesso_ip', 'acesso_data'
    ];

    public function charge()
    {
        return $this->belongsTo(Charge::class, 'did_noticia');
    }
}
