<?php

namespace App\Providers;

use App\News;
use App\Charge;
use App\NewActivity;
use App\Community;

use App\Observers\NewObserver;
use App\Observers\ChargeObserver;
use App\Observers\CommunityObserver;
use App\Observers\NewActivityObserver;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        News::observe(NewObserver::class);
        Charge::observe(ChargeObserver::class);
        Community::observe(CommunityObserver::class);
        NewActivity::observe(NewActivityObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
