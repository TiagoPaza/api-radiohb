<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Community extends Model
{
    protected $table = 'forum_topicos';

    protected $guarded = ['id'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'titulo', 'conteudo', 'editado',
        'editado_autor', 'editado_data', 'cat_id',
        'fechado', 'fechado_motivo', 'fechado_autor',
        'moderado', 'mod_autor',
        'mod_data', 'fixo', 'views', 'status',
        'status_autor', 'data_ativo', 'autor'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['fechado_data', 'status_data', 'data'];

    
    /**
     * We always want new info for orders so eager load to save n+1 queries.
     *
     * @var array
     */
    protected $with = ['category'];


    public function category()
    {
        return $this->belongsTo(NewCategory::class, 'cat_id');
    }
}
