<?php

namespace App\Http\Responses;

use Illuminate\Http\Response;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

use App\User;

use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Serializer\JsonApiSerializer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

use App\Http\Responses\Transformers\UserTransformer;

class UserCollectionResponse extends Response
{
    /**
     * @param Users[]|\Illuminate\Support\Collection $user
     * @param LengthAwarePaginator $paginator
     */
    public function __construct($user, $paginator)
    {
        $fractal = (new Manager())->setSerializer(new JsonApiSerializer());
        $resource = new Collection($user, new UserTransformer(), 'user');
        $resource->setPaginator(new IlluminatePaginatorAdapter($paginator));

        return parent::__construct($fractal->createData($resource)->toArray());
    }
}
