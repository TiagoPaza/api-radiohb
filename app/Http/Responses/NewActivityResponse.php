<?php

namespace App\Http\Responses;

use Illuminate\Http\Response;

use App\NewActivity;

use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\JsonApiSerializer;

use App\Http\Responses\Transformers\NewActivityTransformer;

class NewActivityResponse extends Response
{
    public function __construct($id, $status = Response::HTTP_OK)
    {
        $newActivity = NewActivity::find($id);

        $fractal = (new Manager())->setSerializer(new JsonApiSerializer());
        $resource = new Item($newActivity, new NewActivityTransformer(), 'new_activity');

        return parent::__construct($fractal->createData($resource)->toArray(), $status);
    }
}
