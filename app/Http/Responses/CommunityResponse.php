<?php

namespace App\Http\Responses;

use App\Community;

use Illuminate\Http\Response;

use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\JsonApiSerializer;

use App\Http\Responses\Transformers\CommunityTransformer;

class CommunityResponse extends Response
{
    public function __construct($community, $status = Response::HTTP_OK)
    {
        $fractal = (new Manager())->setSerializer(new JsonApiSerializer());
        $resource = new Item($community, new CommunityTransformer(), 'community');

        return parent::__construct($fractal->createData($resource)->toArray(), $status);
    }
}
