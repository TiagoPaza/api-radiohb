<?php

namespace App\Http\Responses\Transformers;

use App\Community;
use League\Fractal\TransformerAbstract;

class CommunityTransformer extends TransformerAbstract
{
    public function transform(Community $community)
    {
        $meta = isset($community->quantity) ? ['meta' => ['quantity' => $community->quantity]] : [];
        
        return [
            'id' => (string)$community->id,
            'titulo' => $community->titulo,
            'conteudo' => $community->conteudo,
            'fixo' => $community->fixo,
            'status ' => $community->status,
            'autor' => $community->autor,
            'data_publicao' => $community->data
        ] + $meta;
    }
}
