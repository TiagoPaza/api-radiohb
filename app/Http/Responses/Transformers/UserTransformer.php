<?php

namespace App\Http\Responses\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    public function transform(User $user)
    {
        $meta = isset($user->quantity) ? ['meta' => ['quantity' => $user->quantity]] : [];
        
        return [
            'id' => (string)$user->id,
            'nick' => $user->nick,
            'facebook' => $user->facebook,
            'instagram' => $user->instagram,
            'twitter' => $user->twitter,
            'nome' => $user->nome,
            'skype' => $user->skype,
            'programa' => $user->programa,
            'data' => $user->data
        ] + $meta;
    }
}
