<?php

namespace App\Http\Responses\Transformers;

use App\Charge;
use League\Fractal\TransformerAbstract;

class ChargeTransformer extends TransformerAbstract
{
    public function transform(Charge $charge)
    {
        $meta = isset($charge->quantity) ? ['meta' => ['quantity' => $charge->quantity]] : [];

        return [
            'id' => (string)$charge->id,
            'nome' => $charge->nome,
            'descricao' => $charge->descricao,
            'data_publicao' => $charge->data
        ] + $meta;
    }
}
