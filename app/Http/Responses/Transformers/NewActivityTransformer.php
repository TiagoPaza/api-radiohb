<?php

namespace App\Http\Responses\Transformers;

use App\NewActivity;
use League\Fractal\TransformerAbstract;

class NewActivityTransformer extends TransformerAbstract
{
    // /**
    //  * List of resources to automatically include
    //  *
    //  * @var array
    //  */
    // protected $defaultIncludes = [
    //     'new'
    // ];

    public function transform(NewActivity $newActivity)
    {
        return [
            'id' => (string)$newActivity->id,
            'id_noticia' => $newActivity->id_noticia,
            'data_publicao' => $newActivity->data,
            'links' => [
                'self' => '/activities/' . $newActivity->id,
            ]
        ];
    }

    // /**
    //  * Include New
    //  *
    //  * @param NewActivity $newActivity
    //  * @return \League\Fractal\Resource\Item
    //  */
    // public function includeNew(NewActivity $newActivity)
    // {
    //     $newActivity = $newActivity->new;

    //     return $this->item($newActivity, new NewTransformer(), 'new');
    // }
}
