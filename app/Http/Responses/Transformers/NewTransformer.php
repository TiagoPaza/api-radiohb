<?php

namespace App\Http\Responses\Transformers;

use App\News;
use League\Fractal\TransformerAbstract;

class NewTransformer extends TransformerAbstract
{
    public function transform(News $new)
    {
        $meta = isset($new->quantity) ? ['meta' => ['quantity' => $new->quantity]] : [];
        
        return [
            'id' => (string)$new->id,
            'titulo' => $new->titulo,
            'descricao' => $new->descricao,
            'imagem' => 'https://radiohabblet.com' . $new->imagem,
            'noticia' => $new->noticia,
            'categoria' => $new->category->nome,
            'views' => $new->views,
            'autor' => $new->autor,
            'data_publicao' => $new->data
        ] + $meta;
    }
}
