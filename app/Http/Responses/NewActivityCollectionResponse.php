<?php

namespace App\Http\Responses;

use Illuminate\Http\Response;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

use App\NewActivity;

use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Serializer\JsonApiSerializer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

use App\Http\Responses\Transformers\NewActivityTransformer;

class NewActivityCollectionResponse extends Response
{
    /**
     * @param NewActivity[]|\Illuminate\Support\Collection $newActivitys
     * @param LengthAwarePaginator $paginator
     */
    public function __construct($newActivitys, $paginator)
    {
        $fractal = (new Manager())->setSerializer(new JsonApiSerializer());
        $resource = new Collection($newActivitys, new NewActivityTransformer(), 'new_activity');
        $resource->setPaginator(new IlluminatePaginatorAdapter($paginator));

        return parent::__construct($fractal->createData($resource)->toArray());
    }
}
