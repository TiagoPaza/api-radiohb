<?php

namespace App\Http\Responses;

use App\User;

use Illuminate\Http\Response;

use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\JsonApiSerializer;

use App\Http\Responses\Transformers\UserTransformer;

class UserResponse extends Response
{
    public function __construct($user, $status = Response::HTTP_OK)
    {
        $fractal = (new Manager())->setSerializer(new JsonApiSerializer());
        $resource = new Item($user, new UserTransformer(), 'user');

        return parent::__construct($fractal->createData($resource)->toArray(), $status);
    }
}
