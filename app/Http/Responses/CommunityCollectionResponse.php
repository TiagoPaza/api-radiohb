<?php

namespace App\Http\Responses;

use Illuminate\Http\Response;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

use App\Community;

use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Serializer\JsonApiSerializer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

use App\Http\Responses\Transformers\CommunityTransformer;

class CommunityCollectionResponse extends Response
{
    /**
     * @param Community[]|\Illuminate\Support\Collection $community
     * @param LengthAwarePaginator $paginator
     */
    public function __construct($community, $paginator)
    {
        $fractal = (new Manager())->setSerializer(new JsonApiSerializer());
        $resource = new Collection($community, new CommunityTransformer(), 'community');
        $resource->setPaginator(new IlluminatePaginatorAdapter($paginator));

        return parent::__construct($fractal->createData($resource)->toArray());
    }
}
