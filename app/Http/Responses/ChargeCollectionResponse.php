<?php

namespace App\Http\Responses;

use Illuminate\Http\Response;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

use App\Charges;

use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Serializer\JsonApiSerializer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

use App\Http\Responses\Transformers\ChargeTransformer;

class ChargeCollectionResponse extends Response
{
    /**
     * @param Charges[]|\Illuminate\Support\Collection $charges
     * @param LengthAwarePaginator $paginator
     */
    public function __construct($charges, $paginator)
    {
        $fractal = (new Manager())->setSerializer(new JsonApiSerializer());
        $resource = new Collection($charges, new ChargeTransformer(), 'charge');
        $resource->setPaginator(new IlluminatePaginatorAdapter($paginator));

        return parent::__construct($fractal->createData($resource)->toArray());
    }
}
