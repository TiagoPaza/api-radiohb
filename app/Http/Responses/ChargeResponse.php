<?php

namespace App\Http\Responses;

use App\Charge;

use Illuminate\Http\Response;

use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\JsonApiSerializer;

use App\Http\Responses\Transformers\ChargeTransformer;

class ChargeResponse extends Response
{
    public function __construct($chage, $status = Response::HTTP_OK)
    {
        $fractal = (new Manager())->setSerializer(new JsonApiSerializer());
        $resource = new Item($chage, new ChargeTransformer(), 'charge');

        return parent::__construct($fractal->createData($resource)->toArray(), $status);
    }
}
