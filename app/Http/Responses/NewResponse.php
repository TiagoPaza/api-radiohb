<?php

namespace App\Http\Responses;

use App\News;

use Illuminate\Http\Response;

use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\JsonApiSerializer;

use App\Http\Responses\Transformers\NewTransformer;

class NewResponse extends Response
{
    public function __construct($new, $status = Response::HTTP_OK)
    {
        $fractal = (new Manager())->setSerializer(new JsonApiSerializer());
        $resource = new Item($new, new NewTransformer(), 'new');

        return parent::__construct($fractal->createData($resource)->toArray(), $status);
    }
}
