<?php

namespace App\Http\Responses;

use Illuminate\Http\Response;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

use App\News;

use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Serializer\JsonApiSerializer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

use App\Http\Responses\Transformers\NewTransformer;

class NewCollectionResponse extends Response
{
    /**
     * @param News[]|\Illuminate\Support\Collection $news
     * @param LengthAwarePaginator $paginator
     */
    public function __construct($news, $paginator)
    {
        $fractal = (new Manager())->setSerializer(new JsonApiSerializer());
        $resource = new Collection($news, new NewTransformer(), 'new');
        $resource->setPaginator(new IlluminatePaginatorAdapter($paginator));

        return parent::__construct($fractal->createData($resource)->toArray());
    }
}
