<?php

namespace App\Http\Requests;

class NewActivityCreateRequest extends NewActivityRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'data.relationships.notice.data.id' => 'required|exists:notices,id',
        ];
    }
}
