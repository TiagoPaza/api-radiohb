<?php

namespace App\Http\Requests;

abstract class NewRequest extends JsonRequest
{
    public function getTitle()
    {
        return $this->json('data.attributes.titulo');
    }
}
