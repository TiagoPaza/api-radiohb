<?php

namespace App\Http\Requests;

abstract class NewActivityRequest extends JsonRequest
{
    public function getNewId()
    {
        return $this->json('data.relationships.notice.data.id');
    }
}
