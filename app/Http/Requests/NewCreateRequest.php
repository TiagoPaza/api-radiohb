<?php

namespace App\Http\Requests;

class NewCreateRequest extends NewRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'data.attributes.titulo' => 'required|string|between:3,191|unique:notices,titulo'
        ];
    }
}
