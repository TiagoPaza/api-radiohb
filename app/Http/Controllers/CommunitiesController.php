<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Community;

use App\Http\Requests\CommunityCreateRequest;
use App\Http\Requests\CommunityUpdateRequest;

use App\Http\Responses\CommunityResponse;
use App\Http\Responses\CommunityCollectionResponse;

use Illuminate\Pagination\LengthAwarePaginator;

class CommunitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param Community $community
     * @return Response
     */
    public function index(Request $request, Community $community)
    {
        /** @var LengthAwarePaginator $paginator */
        $paginator = $community->paginate($request->input('limit'))->appends($request->query());
        $communities = $paginator->getCollection()->sortByDesc('id');

        return new CommunityCollectionResponse($communities, $paginator);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CommunityCreateRequest $request
     * @param Community $community
     * @return Response
     */
    public function store(CommunityCreateRequest $request, Community $community)
    {
        $community = $community->create(['titulo' => $request->getTitle()]);

        return new CommunityResponse($community, Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Community  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $community = Community::find($id);

        return new CommunityResponse($community);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CommunityUpdateRequest|Request $request
     * @param  \App\Community $community
     * @return Response
     */
    public function update(CommunityUpdateRequest $request, Community $community)
    {
        $community->titulo = $request->getTitle();
        $community->save();

        return new CommunityResponse($community);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Community  $community
     * @return \Illuminate\Http\Response
     */
    public function destroy(Community $community)
    {
        $community->delete();

        return new Response('', Response::HTTP_NO_CONTENT);
    }
}
