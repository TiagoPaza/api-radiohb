<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Charge;

use App\Http\Requests\ChargeCreateRequest;
use App\Http\Requests\ChargeUpdateRequest;

use App\Http\Responses\ChargeResponse;
use App\Http\Responses\ChargeCollectionResponse;

use Illuminate\Pagination\LengthAwarePaginator;

class ChargesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param Charge $charge
     * @return Response
     */
    public function index(Request $request, Charge $charge)
    {
        /** @var LengthAwarePaginator $paginator */
        $paginator = $charge->paginate($request->input('limit'))->appends($request->query());
        $charges = $paginator->getCollection();

        return new ChargeCollectionResponse($charges, $paginator);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ChargeCreateRequest $request
     * @param Charge $charge
     * @return Response
     */
    public function store(ChargeCreateRequest $request, Charge $charge)
    {
        $charge = $charge->create(['titulo' => $request->getTitle()]);

        return new ChargeResponse($charge, Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Charge  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $charge = Charge::find($id);

        return new ChargeResponse($charge);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ChargeUpdateRequest|Request $request
     * @param  \App\Charge $charge
     * @return Response
     */
    public function update(ChargeUpdateRequest $request, Charge $charge)
    {
        $charge->titulo = $request->getTitle();
        $charge->save();

        return new ChargeResponse($charge);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Charge  $charge
     * @return \Illuminate\Http\Response
     */
    public function destroy(Charge $charge)
    {
        $charge->delete();

        return new Response('', Response::HTTP_NO_CONTENT);
    }
}
