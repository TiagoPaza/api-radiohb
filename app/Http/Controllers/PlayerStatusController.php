<?php

namespace App\Http\Controllers;

class PlayerStatusController extends Controller
{
    public function status()
    {
        $host = 'yoda.hosthb.com';
        $port = '9000';
        $ch = curl_init();
        
        curl_setopt($ch, CURLOPT_URL, "http://".$host.":".$port."/");
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla of Habblet Corp");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt_array($ch, array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_HTTPGET => true,
            CURLOPT_HTTPHEADER => array(
                'Cache-Control: no-cache'
            )
        ));

        $data = curl_exec($ch);
        
        $speaker = explode("<font class=default>Stream Title: </font></td><td><font class=default><b>", $data);
        $speaker = explode("</b></td></tr>", $speaker[1]);
        $speaker = strip_tags(addslashes(utf8_encode($speaker[0])));
        
        if ($speaker == '') {
            $speaker = 'Rádio Offline';
        }
        
        $program = explode("<font class=default>Stream Genre: </font></td><td><font class=default>", $data);
        $program = explode("<b>", $program[1]);
        $program = explode("</b></td></tr>", $program[1]);
        $program = strip_tags(addslashes(utf8_encode($program[0])));
        
        if ($program == '') {
            $program = 'Rádio Offline';
        }

        $music = explode("<font class=default>Current Song: </font></td><td><font class=default><b>", $data);
        $music = explode("</b></td></tr>", $music[1]);
        $music = strip_tags(addslashes(utf8_encode($music[0])));
        
        if ($music == '') {
            $music = 'Rádio Offline';
        }

        $unique = explode("<b>Stream is up at ", $data);
        $unique = explode("(", $unique[1]);
        $unique = explode(" unique)", $unique[1]);
        $unique = $unique[0];
        
        if ($unique < 10) {
            $unique = "0" . $unique;
        }

        curl_close($ch);

        $data = [
            'data' => [
                'speaker' => $speaker,
                'program' => $program,
                'unique' => $unique,
                'music' => $music
            ]
        ];

        return response()->json($data);
    }
}
