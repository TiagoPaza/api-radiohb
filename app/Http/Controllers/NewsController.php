<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\News;

use App\Http\Requests\NewCreateRequest;
use App\Http\Requests\NewUpdateRequest;

use App\Http\Responses\NewResponse;
use App\Http\Responses\NewCollectionResponse;

use Illuminate\Pagination\LengthAwarePaginator;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param New $new
     * @return Response
     */
    public function index(Request $request, News $new)
    {
        /** @var LengthAwarePaginator $paginator */
        $paginator = $new->paginate($request->input('limit'))->appends($request->query());
        $news = $paginator->getCollection()->sortByDesc('id');

        return new NewCollectionResponse($news, $paginator);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param NewCreateRequest $request
     * @param New $new
     * @return Response
     */
    public function store(NewCreateRequest $request, News $new)
    {
        $new = $new->create(['titulo' => $request->getTitle()]);

        return new NewResponse($new, Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\New  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $new = News::find($id);

        return new NewResponse($new);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param NewUpdateRequest|Request $request
     * @param  \App\New $new
     * @return Response
     */
    public function update(NewUpdateRequest $request, News $new)
    {
        $new->titulo = $request->getTitle();
        $new->save();

        return new NewResponse($new);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\New  $new
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $new)
    {
        $new->delete();

        return new Response('', Response::HTTP_NO_CONTENT);
    }
}
