<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\NewActivity;

use App\Http\Responses\NewActivityResponse;
use App\Http\Requests\NewActivityCreateRequest;
use App\Http\Requests\NewActivityUpdateRequest;
use App\Http\Responses\NewActivityCollectionResponse;

use Illuminate\Pagination\LengthAwarePaginator;

class NewActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param NewActivity $model
     * @return Response
     */
    public function index(Request $request, NewActivity $model)
    {
        /** @var LengthAwarePaginator $paginator */
        $paginator = $model->paginate($request->input('limit'))->appends($request->query());
        $newActivities = $paginator->getCollection();

        return new NewActivityCollectionResponse($newActivities, $paginator);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param NewActivityCreateRequest $request
     * @param NewActivity $model
     * @return Response
     */
    public function store(NewActivityCreateRequest $request, NewActivity $model)
    {
        $newActivity = $model->create([
            // 'id' => $request->getNewActivityId(),
            'notice_id' => $request->getNewId()
        ]);

        return new NewActivityResponse($newActivity, Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\NewActivity  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new NewActivityResponse($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param NewActivityUpdateRequest $request
     * @param  \App\NewActivity $newActivity
     * @return Response
     */
    public function update(NewActivityUpdateRequest $request, NewActivity $newActivity)
    {
        $newActivity->update(array_filter([
            'id' => $request->getNewActivityId(),
            'quantity' => $request->getQuantity(),
            'notice_id' => $request->getNewId()
        ]));

        return new NewActivityResponse($newActivity);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\NewActivity  $newActivity
     * @return \Illuminate\Http\Response
     */
    public function destroy(NewActivity $newActivity)
    {
        $newActivity->delete();

        return new Response('', Response::HTTP_NO_CONTENT);
    }
}
