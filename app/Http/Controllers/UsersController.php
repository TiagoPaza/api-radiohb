<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\User;

use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;

use App\Http\Responses\UserResponse;
use App\Http\Responses\UserCollectionResponse;

use Illuminate\Pagination\LengthAwarePaginator;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param User $user
     * @return Response
     */
    public function index(Request $request, User $user)
    {
        /** @var LengthAwarePaginator $paginator */
        $paginator = $user->paginate($request->input('limit'))->appends($request->query());
        $users = $paginator->getCollection();
        
        return new UserCollectionResponse($users, $paginator);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UserCreateRequest $request
     * @param User $user
     * @return Response
     */
    public function store(UserCreateRequest $request, User $user)
    {
        $user = $user->create(['titulo' => $request->getTitle()]);

        return new UserResponse($user, Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);

        return new UserResponse($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UserUpdateRequest|Request $request
     * @param  \App\User $user
     * @return Response
     */
    public function update(UserUpdateRequest $request, User $user)
    {
        $user->titulo = $request->getTitle();
        $user->save();

        return new UserResponse($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        return new Response('', Response::HTTP_NO_CONTENT);
    }
}
