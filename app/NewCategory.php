<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class NewCategory extends Model
{
    protected $table = 'noticias_cat';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'nome', 'descricao', 'imagem',
        'home', 'autor', 'status'
    ];
}
