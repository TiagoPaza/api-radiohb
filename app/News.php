<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class News extends Model
{
    protected $table = 'noticias';
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    protected $fillable = [
        'titulo', 'descricao', 'tags',
        'imagem', 'noticia', 'cat_id',
        'comentarios', 'fixo', 'view',
        'views', 'evento', 'evento_horario',
        'blog_id', 'status', 'autor'
    ];
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['data'];

    /**
     * We always want new info for orders so eager load to save n+1 queries.
     *
     * @var array
     */
    protected $with = ['category'];


    public function category()
    {
        return $this->belongsTo(NewCategory::class, 'cat_id');
    }
}
